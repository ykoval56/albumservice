package com.test.album.response;

public class RegisterResponse extends BaseResponse {

  private String username;
  private String userId;
  private String info;

  public RegisterResponse(Head head) {
    super(head);
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }
}
