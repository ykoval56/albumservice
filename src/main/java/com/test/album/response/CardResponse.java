package com.test.album.response;


import com.test.album.core.model.Card;

public class CardResponse extends BaseResponse {

  private Card card;
  private String info;

  public CardResponse(Head head) {
    super(head);
  }

  public Card getCard() {
    return card;
  }

  public void setCard(Card card) {
    this.card = card;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }
}
