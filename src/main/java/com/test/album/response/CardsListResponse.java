package com.test.album.response;

import com.test.album.core.model.Card;
import java.util.List;

public class CardsListResponse extends BaseResponse {

  private List<Card> cards;

  public CardsListResponse(Head head) {
    super(head);
  }

  public List<Card> getCards() {
    return cards;
  }

  public void setCards(List<Card> cards) {
    this.cards = cards;
  }
}
