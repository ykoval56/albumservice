package com.test.album.response;

public class ErrorResponse extends BaseResponse {

  private String error;

  public ErrorResponse(Head head) {
    super(head);
  }

  public ErrorResponse(Head head, String error) {
    super(head);
    this.error = error;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
