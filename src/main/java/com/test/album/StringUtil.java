package com.test.album;

import org.apache.logging.log4j.util.Strings;
import org.apache.tomcat.util.codec.binary.Base64;

public class StringUtil {

  public static String encodeBase64(String string) {
    if (!Strings.isEmpty(string)) {
     byte[] encodedBytes = Base64.encodeBase64(string.getBytes());
     return new String(encodedBytes);
    }
    return null;
  }
}
