package com.test.album.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ErrorControllerImpl implements ErrorController {

  private static final String MESSAGE_400 = "Http Error Code: 400. Bad Request";
  private static final String MESSAGE_401 = "Http Error Code: 401. Unauthorized";
  private static final String MESSAGE_404 = "Http Error Code: 404. Resource not found";
  private static final String MESSAGE_500 = "Http Error Code: 500. Internal Server Error";

  @RequestMapping("/error")
  public String handleError(HttpServletRequest request) {
    int httpErrorCode = (int) request
        .getAttribute("javax.servlet.error.status_code");

    switch (httpErrorCode) {
      case 400:
        return MESSAGE_400;
      case 401:
        return MESSAGE_401;
      case 404:
        return MESSAGE_404;
      case 500:
        return MESSAGE_500;
      default:
        return "";
    }
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}
