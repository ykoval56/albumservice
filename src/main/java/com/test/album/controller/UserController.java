package com.test.album.controller;

import com.test.album.core.interactor.RegisterUserInteractor;
import com.test.album.core.interactor.UsersListInteractor;
import com.test.album.response.BaseResponse;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/accounts")
public class UserController {

  @Autowired
  private ApplicationContext context;

  @RequestMapping("/register")
  public DeferredResult<BaseResponse> registerUser(@RequestParam(value = "uname") String username) {
    DeferredResult<BaseResponse> response = new DeferredResult<>();
    RegisterUserInteractor interactor = context.getBean(RegisterUserInteractor.class);

    Properties properties = new Properties();
    properties.setProperty(RegisterUserInteractor.ARG_USERNAME, username);

    interactor.execute(response, properties);
    return response;
  }

  @RequestMapping("/get_all")
  public DeferredResult<BaseResponse> getAllUsers() {
    DeferredResult<BaseResponse> response = new DeferredResult<>();
    UsersListInteractor interactor = context.getBean(UsersListInteractor.class);

    interactor.execute(response, new Properties());
    return response;
  }
}
