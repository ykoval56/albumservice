package com.test.album.controller;

import com.test.album.core.interactor.CardAchieveInteractor;
import com.test.album.core.interactor.CardCreateInteractor;
import com.test.album.core.interactor.CardListInteractor;
import com.test.album.response.BaseResponse;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/album")
public class AlbumController {

  @Autowired
  private ApplicationContext context;

  @RequestMapping("/achieve")
  public DeferredResult<BaseResponse> achieveCard(@RequestParam(value = "uid") String userId) {
    DeferredResult<BaseResponse> response = new DeferredResult<>();
    CardAchieveInteractor interactor = context.getBean(CardAchieveInteractor.class);

    Properties properties = new Properties();
    properties.setProperty(CardAchieveInteractor.ARG_USER_ID, userId);

    interactor.execute(response, properties);

    return response;
  }

  @RequestMapping("/create_card")
  public DeferredResult<BaseResponse> createCard(@RequestParam(value = "ctitle") String cardTitle,
      @RequestParam(value = "cdesc") String cardDescription, @RequestParam(value = "cuid") String cardUniqueId) {
    DeferredResult<BaseResponse> response = new DeferredResult<>();
    CardCreateInteractor interactor = context.getBean(CardCreateInteractor.class);

    Properties properties = new Properties();
    properties.setProperty(CardCreateInteractor.ARG_CARD_TITLE, cardTitle);
    properties.setProperty(CardCreateInteractor.ARG_CARD_DESCRIPTION, cardDescription);
    properties.setProperty(CardCreateInteractor.ARG_CARD_UNIQUE_ID, cardUniqueId);

    interactor.execute(response, properties);

    return response;
  }

  @RequestMapping("/get_collection")
  public DeferredResult<BaseResponse> getUserCollection(@RequestParam(value = "uid") String userId) {
    DeferredResult<BaseResponse> response = new DeferredResult<>();
    CardListInteractor interactor = context.getBean(CardListInteractor.class);

    Properties properties = new Properties();
    properties.setProperty(CardListInteractor.ARG_USER_ID, userId);

    interactor.execute(response, properties);

    return response;
  }
}
