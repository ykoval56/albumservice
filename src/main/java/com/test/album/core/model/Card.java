package com.test.album.core.model;

public class Card {

  private String cardTitle;
  private String cardDescription;
  private int cardUniqueId;

  public Card(String cardTitle, String cardDescription, int cardUniqueId) {
    this.cardTitle = cardTitle;
    this.cardDescription = cardDescription;
    this.cardUniqueId = cardUniqueId;
  }

  public String getCardTitle() {
    return cardTitle;
  }

  public void setCardTitle(String cardTitle) {
    this.cardTitle = cardTitle;
  }

  public String getCardDescription() {
    return cardDescription;
  }

  public void setCardDescription(String cardDescription) {
    this.cardDescription = cardDescription;
  }

  public int getCardUniqueId() {
    return cardUniqueId;
  }

  public void setCardUniqueId(int cardUniqueId) {
    this.cardUniqueId = cardUniqueId;
  }


  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    if (o.getClass() != this.getClass()) {
      return false;
    }
    int uniqueId = ((Card) o).getCardUniqueId();

    return this.getCardUniqueId() == uniqueId;
  }
}
