package com.test.album.core.repository;

import com.sun.istack.internal.Nullable;
import com.test.album.core.model.Card;
import com.test.album.core.model.User;
import java.util.List;

public interface DataRepository {

  boolean addUser(User user);

  @Nullable
  User getUser(String userId);

  @Nullable
  List<User> getUsers();

  boolean addCard(Card card);

  @Nullable
  Card getCard(int uniqueId);

  @Nullable
  List<Card> getCards();

  boolean addUserCard(User user, Card card);

  @Nullable
  List<Card> getUserCards(String userId);
}
