package com.test.album.core.interactor;

import com.test.album.core.model.Card;
import com.test.album.core.repository.DataRepository;
import com.test.album.response.BaseResponse.Head;
import com.test.album.response.CardResponse;
import com.test.album.response.ErrorResponse;
import io.reactivex.SingleOnSubscribe;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.context.request.async.DeferredResult;

public class CardCreateInteractor extends BaseInteractor<Card> {

  public static final String ARG_CARD_UNIQUE_ID = "arg_card_unique_id";
  public static final String ARG_CARD_TITLE = "arg_card_title";
  public static final String ARG_CARD_DESCRIPTION = "arg_card_description";

  public CardCreateInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  SingleOnSubscribe<Card> interactorSource(Properties args) {
    return emitter -> {
      String cardId = args.getProperty(ARG_CARD_UNIQUE_ID);
      String cardTitle = args.getProperty(ARG_CARD_TITLE);
      String cardDescription = args.getProperty(ARG_CARD_DESCRIPTION);

      if (Strings.isEmpty(cardId) || Strings.isEmpty(cardTitle) || Strings.isEmpty(cardDescription)) {
        emitter.onError(new Exception("Bad parameters."));
        return;
      }

      int cardUniqueId;
      try {
        cardUniqueId = Integer.parseInt(cardId);
      } catch (NumberFormatException e) {
        emitter.onError(new Exception("Incorrect cardId"));
        return;
      }

      synchronized (repository) {
        List<Card> cards = repository.getCards();
        Card newCard = new Card(cardTitle, cardDescription, cardUniqueId);

        if (!cards.contains(newCard)) {
          boolean status = repository.addCard(newCard);
          if (status) {
            emitter.onSuccess(newCard);
          } else {
            emitter.onError(new Exception("Unexpected error"));
          }
        } else {
          emitter.onError(new Exception("Card with this id already exist."));
        }
      }
    };
  }

  @Override
  void onResult(Card card, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    CardResponse response = new CardResponse(head);

    response.setCard(card);
    response.setInfo("Card created.");

    result.setResult(response);
  }

  @Override
  void onFail(Throwable e, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse response = new ErrorResponse(head);

    response.setError(e.getMessage());

    result.setResult(response);
  }
}
