package com.test.album.core.interactor;

import com.test.album.core.repository.DataRepository;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Properties;
import org.springframework.web.context.request.async.DeferredResult;

public abstract class BaseInteractor<M>  {

  protected final DataRepository repository;

  public BaseInteractor(DataRepository repository) {
    this.repository = repository;
  }

  public final void execute(DeferredResult result, Properties args) {
    Single.create(interactorSource(args))
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribeWith(new SingleObserver<M>() {
          @Override
          public void onSubscribe(Disposable disposable) {
          }

          @Override
          public void onSuccess(M model) {
            onResult(model, result);
          }

          @Override
          public void onError(Throwable throwable) {
            onFail(throwable, result);
          }
        });
  }

  abstract SingleOnSubscribe<M> interactorSource(Properties args);

  abstract void onResult(M model, DeferredResult result);

  abstract void onFail(Throwable e, DeferredResult result);
}
