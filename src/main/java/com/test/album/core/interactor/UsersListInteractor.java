package com.test.album.core.interactor;

import com.test.album.core.model.User;
import com.test.album.core.repository.DataRepository;
import com.test.album.response.BaseResponse.Head;
import com.test.album.response.ErrorResponse;
import com.test.album.response.UsersResponse;
import io.reactivex.SingleOnSubscribe;
import java.util.List;
import java.util.Properties;
import org.springframework.web.context.request.async.DeferredResult;

public class UsersListInteractor extends BaseInteractor<List<User>> {

  public UsersListInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  SingleOnSubscribe<List<User>> interactorSource(Properties args) {
    return emitter -> {
      List<User> users = repository.getUsers();
      if (users == null) {
        emitter.onError(new Exception("Unexpected error."));
      } else {
        emitter.onSuccess(users);
      }
    };
  }

  @Override
  void onResult(List<User> users, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    UsersResponse response = new UsersResponse(head);

    response.setUsers(users);

    result.setResult(response);
  }

  @Override
  void onFail(Throwable e, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse response = new ErrorResponse(head);

    response.setError(e.getMessage());

    result.setResult(response);
  }
}
