package com.test.album.core.interactor;

import com.test.album.core.model.Card;
import com.test.album.core.repository.DataRepository;
import com.test.album.response.BaseResponse.Head;
import com.test.album.response.CardsListResponse;
import com.test.album.response.ErrorResponse;
import io.reactivex.SingleOnSubscribe;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.context.request.async.DeferredResult;

public class CardListInteractor extends BaseInteractor<List<Card>> {

  public static final String ARG_USER_ID = "arg_user_id";

  public CardListInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  SingleOnSubscribe<List<Card>> interactorSource(Properties args) {
    return emitter -> {
      String userId = args.getProperty(ARG_USER_ID);
      if (Strings.isEmpty(userId)) {
        emitter.onError(new Exception("Invalid parameter."));
        return;
      }

      List<Card> userCards = repository.getUserCards(userId);
      if (userCards != null) {
        emitter.onSuccess(userCards);
      } else {
        emitter.onError(new Exception("Unexpected error."));
      }
    };
  }

  @Override
  void onResult(List<Card> userCards, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    CardsListResponse response = new CardsListResponse(head);
    response.setCards(userCards);

    result.setResult(response);
  }

  @Override
  void onFail(Throwable e, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse response = new ErrorResponse(head);
    response.setError(e.getMessage());

    result.setResult(response);
  }

}
