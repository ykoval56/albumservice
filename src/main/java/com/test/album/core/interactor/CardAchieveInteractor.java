package com.test.album.core.interactor;

import com.sun.istack.internal.Nullable;
import com.test.album.core.model.Card;
import com.test.album.core.model.User;
import com.test.album.core.repository.DataRepository;
import com.test.album.response.BaseResponse.Head;
import com.test.album.response.CardResponse;
import com.test.album.response.ErrorResponse;
import io.reactivex.SingleOnSubscribe;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import javax.validation.constraints.NotNull;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.context.request.async.DeferredResult;

public class CardAchieveInteractor extends BaseInteractor<Card> {

  public static final String ARG_USER_ID = "arg_user_id";

  public CardAchieveInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  SingleOnSubscribe<Card> interactorSource(@NotNull Properties args) {
    return emitter -> {

      String userId = args.getProperty(ARG_USER_ID);
      if (Strings.isEmpty(userId)) {
        emitter.onError(new Exception("Invalid parameter."));
        return;
      }
      User user = repository.getUser(userId);
      if (user == null) {
        emitter.onError(new Exception("User with this username is not found."));
        return;
      }

      if (shouldAchieveCard()) {
        List<Card> cards = repository.getCards();

        synchronized (repository) {
          List<Card> userCards = repository.getUserCards(user.getUserId());

          if (cards == null || userCards == null) {
            emitter.onError(new Exception("Unexpected error."));
            return;
          } else if (cards.isEmpty()) {
            emitter.onError(new Exception("Card collection is empty."));
            return;
          }

          Card card = this.getUnachievedCard(userCards, cards);
          if (card == null) {
            emitter.onError(new Exception("User already collected all cards"));
            return;
          }

          boolean result = repository.addUserCard(user, card);
          if (result) {
            emitter.onSuccess(card);
          } else {
            emitter.onError(new Throwable("Unexpected error"));
          }
        }

      } else {
        if (!emitter.isDisposed()) {
          emitter.onError(new Exception("User did not achieve card."));
        }
      }
    };
  }

  @Override
  void onResult(Card card, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    CardResponse response = new CardResponse(head);

    response.setCard(card);
    response.setInfo("User was achieved card.");

    result.setResult(response);
  }

  @Override
  void onFail(Throwable e, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse response = new ErrorResponse(head);

    response.setError(e.getMessage());

    result.setResult(response);
  }

  private boolean shouldAchieveCard() {
    Random random = new Random();
    return random.nextBoolean();
  }

  @Nullable
  private Card getUnachievedCard(List<Card> achievedCards, List<Card> cardList) {
    for (Card card : cardList) {
      if (!achievedCards.contains(card)) {
        return card;
      }
    }
    return null;
  }
}
