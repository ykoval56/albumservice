package com.test.album.core.interactor;

import com.test.album.StringUtil;
import com.test.album.core.model.User;
import com.test.album.core.repository.DataRepository;
import com.test.album.response.BaseResponse.Head;
import com.test.album.response.ErrorResponse;
import com.test.album.response.RegisterResponse;
import io.reactivex.SingleOnSubscribe;
import java.util.Properties;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.context.request.async.DeferredResult;

public class RegisterUserInteractor extends BaseInteractor<User> {

  public static final String ARG_USERNAME = "arg_username";

  public RegisterUserInteractor(DataRepository repository) {
    super(repository);
  }

  @Override
  SingleOnSubscribe<User> interactorSource(Properties args) {
    return emitter -> {
      String username = args.getProperty(ARG_USERNAME);
      if (!Strings.isEmpty(username)) {

        User user = new User();
        String userId = StringUtil.encodeBase64(username);

        user.setUsername(username);
        user.setUserId(userId);

        if (isRegistered(user)) {
          emitter.onError(new Exception("User with this username already exist."));
          return;
        }

        boolean result = repository.addUser(user);
        if (result) {
          emitter.onSuccess(user);
        } else {
          emitter.onError(new Exception("Unexpected error."));
        }
      } else {
        emitter.onError(new Exception("Invalid parameter."));
      }
    };
  }

  @Override
  void onResult(User user, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    RegisterResponse registerResponse = new RegisterResponse(head);

    registerResponse.setUserId(user.getUserId());
    registerResponse.setUsername(user.getUsername());
    registerResponse.setInfo("New user registered.");

    result.setResult(registerResponse);
  }

  @Override
  void onFail(Throwable e, DeferredResult result) {
    Head head = new Head(System.currentTimeMillis());
    ErrorResponse errorResponse = new ErrorResponse(head, e.getMessage());

    result.setResult(errorResponse);
  }


  private synchronized boolean isRegistered(User user) {
    User oldUser = repository.getUser(user.getUserId());
    return oldUser != null;
  }
}
