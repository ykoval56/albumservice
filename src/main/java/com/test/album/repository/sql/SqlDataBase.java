package com.test.album.repository.sql;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SqlDataBase {

  private Connection connection;

  public Connection getConnection() {
    try {
      if (this.connection != null && !this.connection.isClosed()) {
        return this.connection;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    Properties properties = getDbProperties();

    if (properties == null) {
      return null;
    }

    String link = buildConnectionLink(properties.getProperty("host"), properties.getProperty("dbName"),
        properties.getProperty("dbType"), Integer.valueOf(properties.getProperty("port")));

    try {
      this.connection = DriverManager.getConnection(link, properties);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
    return this.connection;
  }

  private Properties getDbProperties() {
    String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
    String iconConfigPath = rootPath + "properties.xml";

    try {
      Properties iconProps = new Properties();
      iconProps.loadFromXML(new FileInputStream(iconConfigPath));
      return iconProps;
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }


  private static String buildConnectionLink(String host, String dbName, String dbType, int port) {
    return "jdbc:" + dbType + "://" + host + ":" + port + "/" + dbName;
  }
}
