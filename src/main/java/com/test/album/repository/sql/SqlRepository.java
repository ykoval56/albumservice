package com.test.album.repository.sql;

import com.test.album.core.model.Card;
import com.test.album.core.model.User;
import com.test.album.core.repository.DataRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlRepository implements DataRepository {

  private static final String DELIMITER = ",";
  private static final String QUOTE = "'";

  private SqlDataBase dataBase;

  public SqlRepository(SqlDataBase dataBase) {
    this.dataBase = dataBase;
    this.prepareDataBase();
  }

  @Override
  public boolean addUser(User user) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return false;
    }

    StringBuilder sb = new StringBuilder()
        .append("INSERT INTO users(username, user_id)")
        .append(" VALUES (")
        .append(QUOTE)
        .append(user.getUsername())
        .append(QUOTE + DELIMITER + QUOTE)
        .append(user.getUserId())
        .append(QUOTE + ")");

    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(sb.toString());
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public User getUser(String userId) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder()
        .append("SELECT * FROM users ")
        .append("WHERE user_id=")
        .append(QUOTE)
        .append(userId)
        .append(QUOTE);

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sb.toString());

      String username = null;
      while (resultSet.next()) {
        username = resultSet.getString("username");
      }
      if (username == null || username.isEmpty()) {
        return null;
      }
      return new User(username, userId);

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<User> getUsers() {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return null;
    }
    List<User> users = new ArrayList<>();

    String query = "SELECT * FROM users";

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        String username = resultSet.getString("username");
        String userId = resultSet.getString("user_id");

        users.add(new User(username, userId));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
    return users;
  }

  @Override
  public boolean addCard(Card card) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return false;
    }

    StringBuilder sb = new StringBuilder()
        .append("INSERT INTO cards(c_title, c_description, c_unique_id)")
        .append(" VALUES (")
        .append(QUOTE)
        .append(card.getCardTitle())
        .append(QUOTE + DELIMITER + QUOTE)
        .append(card.getCardDescription())
        .append(QUOTE + DELIMITER)
        .append(card.getCardUniqueId())
        .append(")");

    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(sb.toString());
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public Card getCard(int uniqueId) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder()
        .append("SELECT * FROM cards ")
        .append("WHERE c_unique_id=")
        .append(uniqueId);

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sb.toString());

      Card card = null;
      while (resultSet.next()) {
        String cardTitle = resultSet.getString("c_title");
        String cardDescription = resultSet.getString("c_description");

        card = new Card(cardTitle, cardDescription, uniqueId);
      }
      return card;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<Card> getCards() {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return null;
    }
    List<Card> cards = new ArrayList<>();

    String query = "SELECT * FROM cards";

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        String cardTitle = resultSet.getString("c_title");
        String cardDescription = resultSet.getString("c_description");
        int cardUniqueId = resultSet.getInt("c_unique_id");

        cards.add(new Card(cardTitle, cardDescription, cardUniqueId));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
    return cards;
  }

  @Override
  public boolean addUserCard(User user, Card card) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return false;
    }

    StringBuilder sb = new StringBuilder()
        .append("INSERT INTO users_cards(user_id, card_id) ")
        .append("VALUES (")
        .append(QUOTE)
        .append(user.getUserId())
        .append(QUOTE + DELIMITER + QUOTE)
        .append(card.getCardUniqueId())
        .append(QUOTE + ")");

    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(sb.toString());
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public List<Card> getUserCards(String userId) {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return null;
    }
    List<Card> userCards = new ArrayList<>();

    StringBuilder sb = new StringBuilder()
        .append("SELECT * FROM cards ")
        .append("INNER JOIN users_cards ")
        .append("ON cards.c_unique_id = users_cards.card_id ")
        .append("WHERE user_id=")
        .append(QUOTE)
        .append(userId)
        .append(QUOTE);

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(sb.toString());

      while (resultSet.next()) {
        String cardTitle = resultSet.getString("c_title");
        String cardDescription = resultSet.getString("c_description");
        int cardUniqueId = resultSet.getInt("c_unique_id");

        userCards.add(new Card(cardTitle, cardDescription, cardUniqueId));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
    return userCards;
  }

  private void prepareDataBase() {
    Connection connection = this.dataBase.getConnection();
    if (connection == null) {
      return;
    }

    StringBuilder usersTable = new StringBuilder()
        .append("CREATE TABLE IF NOT EXISTS users (")
        .append("id INT AUTO_INCREMENT PRIMARY KEY")
        .append(DELIMITER)
        .append("username VARCHAR(30) NOT NULL")
        .append(DELIMITER)
        .append("user_id VARCHAR(100) NOT NULL)");

    StringBuilder cardsTable = new StringBuilder()
        .append("CREATE TABLE IF NOT EXISTS cards (")
        .append("id INT AUTO_INCREMENT PRIMARY KEY")
        .append(DELIMITER)
        .append("c_title VARCHAR(100)")
        .append(DELIMITER)
        .append("c_description VARCHAR(255)")
        .append(DELIMITER)
        .append("c_unique_id INT NOT NULL)");

    StringBuilder userCardsTable = new StringBuilder()
        .append("CREATE TABLE IF NOT EXISTS users_cards (")
        .append("id INT AUTO_INCREMENT PRIMARY KEY")
        .append(DELIMITER)
        .append("user_id VARCHAR(100) NOT NULL")
        .append(DELIMITER)
        .append("card_id INT NOT NULL)");

    try {
      Statement statement = connection.createStatement();

      statement.execute(usersTable.toString());
      statement.execute(cardsTable.toString());
      statement.execute(userCardsTable.toString());

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
